# -*- coding: utf-8 -*-
# This file is part of OpenERP. The COPYRIGHT file at the top level of
# this module contains the full copyright notices and license terms.
{
    'name' : 'Development Helper',
    'version': '0.1',
    'author': 'HBee UAB',
    'category': 'Custom',
    'website': 'http://www.hbee.eu',
    'summary': '',
    'description': """
Some features which makes OpenERP more developer friendly.
===========================================================
This module is installed automatically
""",
    'depends': [
                'base',
    ],
    'data': [
             'data/data.xml',
             'view/module_view.xml',
    ],
    'installable': True,
    'auto_install': True,
    'application': False,
}
