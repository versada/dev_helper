# -*- coding: utf-8 -*-
# This file is part of OpenERP. The COPYRIGHT file at the top level of
# this module contains the full copyright notices and license terms.

from openerp.osv import fields, orm


class MrpProduction(orm.Model):
    _inherit = 'ir.module.module'

    def _button_immediate_function(self, cr, uid, ids, function, context=None):
        """
        Override this method in order to prevent page reloading after module action
        """
        super(MrpProduction, self)._button_immediate_function(cr, uid, ids, function, context=context)
        return True